/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio.c								*/
/*                                                          */
/* ******************************************************** */


#include "Dio.h"
#include "MemMap.h"
#include "Dem.h"
#include "Det.h"

Std_VersionInfoType Dio_Version;


Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId)
{
	Dio_LevelType RetVar, TempVar;
#if DIO_DEV_ERROR_DETECT == 1
	if(ChannelId >= DIO_CHANNELS_NUMBER)
	{
		Det_ReportError();
		RetVar = STD_LOW;
	}
	else
	{
#endif
		if((ChannelId >= DIO_CHANNEL_A_1 &&
				ChannelId <= DIO_CHANNEL_A_8))
		{
			TempVar = GET_BIT(DIO_uint8_PINA,ChannelId);
		}
		else if((ChannelId >= DIO_CHANNEL_B_1 &&
				ChannelId <= DIO_CHANNEL_B_8))
		{
			TempVar = GET_BIT(DIO_uint8_PINB,ChannelId-EIGHT);
		}
		else if((ChannelId >= DIO_CHANNEL_C_1 &&
				ChannelId <= DIO_CHANNEL_C_8))
		{
			TempVar = GET_BIT(DIO_uint8_PINC,ChannelId-SIXTEEN);
		}
		else if((ChannelId >= DIO_CHANNEL_D_1 &&
				ChannelId <= DIO_CHANNEL_D_8))
		{
			TempVar = GET_BIT(DIO_uint8_PIND,ChannelId-TWENTY_FOUR);
		}

		if(TempVar == 0)
		{
			RetVar = STD_LOW;
		}
		else if(TempVar == 1)
		{
			RetVar = STD_HIGH;
		}
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif
	return RetVar;
}



void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level)
{
#if DIO_DEV_ERROR_DETECT == 1
	if((ChannelId >= DIO_CHANNELS_NUMBER) || (Level != STD_LOW) || (Level != STD_HIGH))
	{
		Det_ReportError();
	}
	else
	{
#endif

		switch(Level)
		{
		case STD_LOW:

			if((ChannelId >= DIO_CHANNEL_A_1 &&
					ChannelId <= DIO_CHANNEL_A_8))
			{
				CLR_BIT(DIO_uint8_PORTA,ChannelId);
			}
			else if((ChannelId >= DIO_CHANNEL_B_1 &&
					ChannelId <= DIO_CHANNEL_B_8))
			{
				CLR_BIT(DIO_uint8_PORTB,ChannelId-EIGHT);
			}
			else if((ChannelId >= DIO_CHANNEL_C_1 &&
					ChannelId <= DIO_CHANNEL_C_8))
			{
				CLR_BIT(DIO_uint8_PORTC,ChannelId-SIXTEEN);
			}
			else if((ChannelId >= DIO_CHANNEL_D_1 &&
					ChannelId <= DIO_CHANNEL_D_8))
			{
				CLR_BIT(DIO_uint8_PORTD,ChannelId-TWENTY_FOUR);
			}
			break;


		case STD_HIGH:

			if((ChannelId >= DIO_CHANNEL_A_1 &&
					ChannelId <= DIO_CHANNEL_A_8))
			{
				SET_BIT(DIO_uint8_DDRA,ChannelId);
			}
			else if((ChannelId >= DIO_CHANNEL_B_1 &&
					ChannelId <= DIO_CHANNEL_B_8))
			{
				SET_BIT(DIO_uint8_DDRB,ChannelId-EIGHT);
			}
			else if((ChannelId >= DIO_CHANNEL_C_1 &&
					ChannelId <= DIO_CHANNEL_C_8))
			{
				SET_BIT(DIO_uint8_DDRC,ChannelId-SIXTEEN);
			}
			else if((ChannelId >= DIO_CHANNEL_D_8 &&
					ChannelId <= DIO_CHANNEL_D_8))
			{
				SET_BIT(DIO_uint8_DDRD,ChannelId-TWENTY_FOUR);
			}
			break;
		}

		return;
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif

	return;
}

Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId)
{
	Dio_PortLevelType RetVar;
#if DIO_DEV_ERROR_DETECT == 1
	if(PortId >= DIO_PORTS_NUMBER)
	{
		Det_ReportError();
	}
	else
	{
#endif
		switch(PortId)
		{
			case DIO_PORT_A:
				RetVar = GET_BYTE(DIO_uint8_PINA);
				break;
			case DIO_PORT_B:
				RetVar = GET_BYTE(DIO_uint8_PINB);
				break;
			case DIO_PORT_C:
				RetVar = GET_BYTE(DIO_uint8_PINC);
				break;
			case DIO_PORT_D:
				RetVar = GET_BYTE(DIO_uint8_PIND);
				break;
		}
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif
	return RetVar;
}


void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level)
{
#if DIO_DEV_ERROR_DETECT == 1
	if(PortId >= DIO_PORTS_NUMBER)
	{
		Det_ReportError();
	}
	else
	{
#endif
		switch(PortId)
		{
		case DIO_PORT_A:
			ASG_BYTE(DIO_uint8_PORTA, Level);
			break;
		case DIO_PORT_B:
			ASG_BYTE(DIO_uint8_PORTB, Level);
			break;
		case DIO_PORT_C:
			ASG_BYTE(DIO_uint8_PORTC, Level);
			break;
		case DIO_PORT_D:
			ASG_BYTE(DIO_uint8_PORTD, Level);
			break;
		}
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif
	return;
}


Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupPtr)
{
	Dio_PortLevelType RetVar, TempVar;
	Dio_PortType Port;

#if DIO_DEV_ERROR_DETECT == 1
	if(ChannelGroupPtr == NULL_PTR)
	{
		Det_ReportError();
	}
	else
	{
#endif
		switch(ChannelGroupPtr->port)
		{
			case DIO_PORT_A:
				Port = DIO_uint8_PINA;
				break;
			case DIO_PORT_B:
				Port = DIO_uint8_PINB;
				break;
			case DIO_PORT_C:
				Port = DIO_uint8_PINC;
				break;
			case DIO_PORT_D:
				Port = DIO_uint8_PIND;
				break;
		}
		TempVar = (Port) & (ChannelGroupPtr->mask);
		RetVar = (TempVar) >> (ChannelGroupPtr->offset);
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif

	return RetVar;
}
void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupPtr, Dio_PortLevelType Level)
{
	Dio_PortLevelType TempVar;
	Dio_PortType Port;
#if DIO_DEV_ERROR_DETECT == 1
	if(ChannelGroupPtr == NULL_PTR)
	{
		Det_ReportError();
	}
	else
	{
#endif
		switch(ChannelGroupPtr->port)
		{
			case DIO_PORT_A:
				Port = DIO_uint8_PORTA;
				break;
			case DIO_PORT_B:
				Port = DIO_uint8_PORTB;
				break;
			case DIO_PORT_C:
				Port = DIO_uint8_PORTC;
				break;
			case DIO_PORT_D:
				Port = DIO_uint8_PORTD;
				break;
		}

		/*	Clearing the masked channels 						*/
		Port = Port & (~(ChannelGroupPtr->mask));
		/*	Shifting the level by the offset					*/
		TempVar = (Level) << (ChannelGroupPtr->offset);
		/*	Writing the shifted level into the masked channels	*/
		Port |= TempVar;

#if DIO_DEV_ERROR_DETECT == 1
	}
#endif
	return;
}



#if DIO_VERSION_INFO_API == 1
void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo)
{
#if DIO_DEV_ERROR_DETECT == 1
	if(VersionInfo == NULL_PTR)
	{
		// Raise DIO_E_PARAM_POINTER
	}
	else
	{
#endif

		VersionInfo = &Dio_Version;

#if DIO_DEV_ERROR_DETECT == 1
	}
#endif

	return;
}
#endif
#if DIO_FLIP_CHANNEL_API == 1
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId)
{
	Dio_LevelType RetVar;
#if DIO_DEV_ERROR_DETECT == 1
	if(ChannelId >= DIO_CHANNELS_NUMBER)
	{
		Det_ReportError();
	}
	else
	{
#endif
		if((ChannelId >= DIO_CHANNEL_A_1 &&
				ChannelId <= DIO_CHANNEL_A_8))
		{
			TOG_BIT(DIO_uint8_PORTA,ChannelId);
		}
		else if((ChannelId >= DIO_CHANNEL_B_1 &&
				ChannelId <= DIO_CHANNEL_B_8))
		{
			TOG_BIT(DIO_uint8_PORTB,ChannelId-EIGHT);
		}
		else if((ChannelId >= DIO_CHANNEL_C_1 &&
				ChannelId <= DIO_CHANNEL_C_8))
		{
			TOG_BIT(DIO_uint8_PORTC,ChannelId-SIXTEEN);
		}
		else if((ChannelId >= DIO_CHANNEL_D_1 &&
				ChannelId <= DIO_CHANNEL_D_8))
		{
			TOG_BIT(DIO_uint8_PORTD,ChannelId-TWENTY_FOUR);
		}
#if DIO_DEV_ERROR_DETECT == 1
	}
#endif

	return RetVar;

}
#endif
