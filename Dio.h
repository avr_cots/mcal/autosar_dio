/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio.h								*/
/*                                                          */
/* ******************************************************** */

#ifndef DIO_H_
#define DIO_H_

#include "Std_Types.h"
#include "Dio_Cfg.h"

typedef uint8	Dio_ChannelType;

typedef uint8	Dio_PortType;

typedef struct
{
	uint8 mask;
	uint8 offset;
	Dio_PortType port;

}Dio_ChannelGroupType;

/*	This variable shouldn't be here, instead it should be in the Dio_Cfg.h file, */
/*	but the project won't compile unless it's put here	*/
extern const Dio_ChannelGroupType DioConfigData[1];

typedef uint8 Dio_LevelType;
#define STD_LOW		0x00
#define STD_HIGH	0x01

typedef uint8 Dio_PortLevelType;

typedef struct
{

}Dio_ConfigType;


Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId);
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level);
Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupPtr);
void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupPtr, Dio_PortLevelType Level);
#if DIO_VERSION_INFO_API == 1
void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo);
#endif
void Dio_Init(const Dio_ConfigType* ConfigPtr);
#if DIO_FLIP_CHANNEL_API == 1
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId);
#endif


#endif /* DIO_H_ */
