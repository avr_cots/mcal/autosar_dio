/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio_Cfg.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#include "Dio_Pins.h"
#include "Dio_Ports.h"

#define	DIO_DEV_ERROR_DETECT	0
#define DIO_FLIP_CHANNEL_API	1
#define DIO_VERSION_INFO_API	1


#define MOTOR_START_STOP		(DIO_CHANNEL_A_5)

#define MOTOR_CTL_PORT			(DIO_PORT_A)

#define MOTOR_CTR_GRP_PTR		(&DioConfigData[0])



#endif /* DIO_CFG_H_ */
