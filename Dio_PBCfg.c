/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio_PBCfg.c							*/
/*                                                          */
/* ******************************************************** */

#include "Dio.h"

const Dio_ChannelGroupType DioConfigData[1] =
{
		{
				0x1E,
				1,
				DIO_PORT_A
		}
};
