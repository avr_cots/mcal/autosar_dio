/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio_Pins.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef DIO_PINS_H_
#define DIO_PINS_H_

/* Number of Dio Channels in ATMEGA32 uC */
#define DIO_CHANNELS_NUMBER		(uint8)32

/*		PORT A Channels		*/
#define DIO_CHANNEL_A_1			(Dio_ChannelType)0
#define DIO_CHANNEL_A_2			(Dio_ChannelType)1
#define DIO_CHANNEL_A_3			(Dio_ChannelType)2
#define DIO_CHANNEL_A_4			(Dio_ChannelType)3
#define DIO_CHANNEL_A_5			(Dio_ChannelType)4
#define DIO_CHANNEL_A_6			(Dio_ChannelType)5
#define DIO_CHANNEL_A_7			(Dio_ChannelType)6
#define DIO_CHANNEL_A_8			(Dio_ChannelType)7

/*		PORT B Channels		*/
#define DIO_CHANNEL_B_1			(Dio_ChannelType)8
#define DIO_CHANNEL_B_2			(Dio_ChannelType)9
#define DIO_CHANNEL_B_3			(Dio_ChannelType)10
#define DIO_CHANNEL_B_4			(Dio_ChannelType)11
#define DIO_CHANNEL_B_5			(Dio_ChannelType)12
#define DIO_CHANNEL_B_6			(Dio_ChannelType)13
#define DIO_CHANNEL_B_7			(Dio_ChannelType)14
#define DIO_CHANNEL_B_8			(Dio_ChannelType)15

/*		PORT C Channels		*/
#define DIO_CHANNEL_C_1			(Dio_ChannelType)16
#define DIO_CHANNEL_C_2			(Dio_ChannelType)17
#define DIO_CHANNEL_C_3			(Dio_ChannelType)18
#define DIO_CHANNEL_C_4			(Dio_ChannelType)19
#define DIO_CHANNEL_C_5			(Dio_ChannelType)20
#define DIO_CHANNEL_C_6			(Dio_ChannelType)21
#define DIO_CHANNEL_C_7			(Dio_ChannelType)22
#define DIO_CHANNEL_C_8			(Dio_ChannelType)23

/*		PORT D Channels		*/
#define DIO_CHANNEL_D_1			(Dio_ChannelType)24
#define DIO_CHANNEL_D_2			(Dio_ChannelType)25
#define DIO_CHANNEL_D_3			(Dio_ChannelType)26
#define DIO_CHANNEL_D_4			(Dio_ChannelType)27
#define DIO_CHANNEL_D_5			(Dio_ChannelType)28
#define DIO_CHANNEL_D_6			(Dio_ChannelType)29
#define DIO_CHANNEL_D_7			(Dio_ChannelType)30
#define DIO_CHANNEL_D_8			(Dio_ChannelType)31



/*		Some private MACROs			*/
#define	EIGHT					(uint8)8
#define	SIXTEEN					(uint8)16
#define	TWENTY_FOUR				(uint8)24


/* ******************************************************************** */
/* This macro is used for setting a certain bit in a specific variable  */
/* Inputs: var: 	Variable to be changed								*/
/*		   bitNo:	Number of bit to be set								*/
/* ******************************************************************** */
#define SET_BIT(var,bitNo) ((var) |=  (1<<(bitNo)))


/* ******************************************************************** */
/* This macro is used for clearing a certain bit in a specific variable */
/* Inputs: var: 	Variable to be changed								*/
/*		   bitNo:	Number of bit to be cleared							*/
/* ******************************************************************** */
#define CLR_BIT(var,bitNo) ((var) &= ~(1<<(bitNo)))


/* ******************************************************************** */
/* This macro is used for toggling a certain bit in a specific variable */
/* Inputs: var: 	Variable to be changed							                  */
/*		   bitNo:	Number of bit to be toggled						                  */
/* ******************************************************************** */
#define TOG_BIT(var,bitNo) ((var) ^=  (1<<(bitNo)))


/* ************************************************************************************ */
/* This macro is used for getting the value of a certain bit in a specific variable  	*/
/* Inputs: var: 	Variable to be changed												*/
/*		   bitNo:	Number of bit to be get												*/
/* ************************************************************************************ */
#define GET_BIT(var,bitNo)		 ((var&(1<<(bitNo)))>>(bitNo))


/* ************************************************************************************ */
/* This macro is used for getting the value of a variable  								*/
/* Inputs: var: 	Variable to be read													*/
/* ************************************************************************************ */
#define GET_BYTE(var)					((var))


/* ************************************************************************************ */
/* This macro is used for assigning a variable with a certain value					  	*/
/* Inputs: var: 	Variable to be changed												*/
/*		   value:	Value to be assigned												*/
/* ************************************************************************************ */
#define ASG_BYTE(var,value)				((var) = (value))

#endif /* DIO_PINS_H_ */
