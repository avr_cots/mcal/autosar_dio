/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019                		*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			DIO									*/
/*   Version :          V01	                                */
/*	 File	 :			Dio_Ports.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef DIO_PORTS_H_
#define DIO_PORTS_H_

/* Number of Dio Ports in ATMEGA 32 uC */
#define DIO_PORTS_NUMBER	(uint8)4

#define DIO_PORT_A			(Dio_PortType)0
#define DIO_PORT_B			(Dio_PortType)1
#define DIO_PORT_C			(Dio_PortType)2
#define DIO_PORT_D			(Dio_PortType)3


/*	Physical Ports	*/

/* Defining registers of PORT A */
#define DIO_uint8_PORTA		*((uint8*)0x3B)
#define DIO_uint8_DDRA 		*((uint8*)0x3A)
#define DIO_uint8_PINA		*((uint8*)0x39)

/* Defining registers of PORT B */
#define DIO_uint8_PORTB		*((uint8*)0x38)
#define DIO_uint8_DDRB		*((uint8*)0x37)
#define DIO_uint8_PINB		*((uint8*)0x36)

/* Defining registers of PORT C */
#define DIO_uint8_PORTC		*((uint8*)0x35)
#define DIO_uint8_DDRC		*((uint8*)0x34)
#define DIO_uint8_PINC		*((uint8*)0x33)

/* Defining registers of PORT D */
#define DIO_uint8_PORTD		*((uint8*)0x32)
#define DIO_uint8_DDRD		*((uint8*)0x31)
#define DIO_uint8_PIND		*((uint8*)0x30)

#endif /* DIO_PORTS_H_ */
